﻿using UnityEngine;
using System.Collections;

//ATTACH TO MAIN CAMERA, shows your health and coins
using UnityEngine.UI;


public class Menu : Singleton <Menu>
{	
	public Text HealthText;
	public Text DistanceText;
	public Text ScoreText;

	[SerializeField]
	ScoreManager scoreManager;
	[SerializeField]
	GameObject hiscoreUI;

	private int distance;
	float timedoRestart=5;
	bool doRestart=false;
	public int Distance {
		get {
			return distance;
		}
		set {
			distance = value;
			DistanceText.text = distance.ToString();
			updatePlayer ();
		}
	}

	private int score;

	public int Score {
		get {
			return score;
		}
		set {
			score = value;
			ScoreText.text = score.ToString();
			updatePlayer ();
		}
	}
	void Update()
	{
		if (doRestart) {
			timedoRestart -= Time.deltaTime;
			if (timedoRestart<0)
				Application.LoadLevel (0);
		}
	}
	void updatePlayer()
	{
		scoreManager.SetScore("player4", "kills", score);
		scoreManager.SetScore("player4", "deaths", distance);
		}

	[HideInInspector]
	public int coinsCollected;

	private int coinsInLevel;
	private Health health;
	
	//setup, get how many coins are in this level
	void Start()
	{
		int value = 0;
		Score = 0;
		scoreManager.changeCounter++;
		for (int index=0;index<5;index++)
		{
			value = PlayerPrefs.GetInt ("top" + index.ToString () + "score",0);
			scoreManager.SetScore("top"+index.ToString(), "kills",value);
			value=PlayerPrefs.GetInt ("top" + index.ToString () + "distance",0);//, scoreManager.GetScore (name, "kills").ToString ());
			scoreManager.SetScore("top"+index.ToString(),"deaths",value);//, scoreManager.GetScore (name, "deaths").ToString ());
		}
		scoreManager.changeCounter++;
	}
	public void Save()
	{
		int value = 0;
		Vector3 pos = hiscoreUI.transform.localPosition;
		pos.y = 0;
		hiscoreUI.transform.localPosition=pos;
			 
		scoreManager.changeCounter++;
		string[] names = scoreManager.GetPlayerNames ("kills");

			for (int index=0;index<5;index++)
			{
			value = scoreManager.GetScore (names[index], "kills");
			PlayerPrefs.SetInt("top" + index.ToString () + "score", value);
			value=PlayerPrefs.GetInt ("top" + index.ToString () + "score");
			value=scoreManager.GetScore(names[index], "deaths");
			PlayerPrefs.SetInt("top" + index.ToString () + "distance" , value);

			//	("player"+index.ToString(), "kills",PlayerPrefs.GetInt ("player" + index.ToString () + "score"));//, scoreManager.GetScore (name, "kills").ToString ());
			//	scoreManager.SetScore("player"+index.ToString(),"deaths",PlayerPrefs.GetInt ("player" + index.ToString () + "distance"));//, scoreManager.GetScore (name, "deaths").ToString ());
			}
		doRestart = true;
	}

}
﻿using UnityEngine;
using System.Collections;

public class moveBullet : MonoBehaviour {
	[SerializeField]
	float bulletspeed=10;
	float lifeTime=4;
	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, lifeTime);
					}
	// Update is called once per frame
	void Update () {
		transform.Translate (bulletspeed * Time.deltaTime, 0, 0);
	}
//	void OnTriggerEnter(Collider other)
//	{
//		if (other.tag == "Enemy")
//			Destroy (other.gameObject);
//		Menu.Instance.Score++;// .text = value.ToString();
//	}
	void OnCollisionEnter(Collision col)
	{
		Debug.Log (col.gameObject.tag);
		if (col.gameObject.tag == "Enemy") {
			col.gameObject.GetComponent<Health> ().playDeath ();
			col.gameObject.SetActive (false);
		}
		Menu.Instance.Score++;

		Destroy (this.gameObject);
	}
}

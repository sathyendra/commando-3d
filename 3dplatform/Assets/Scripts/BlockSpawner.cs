﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockSpawner : MonoBehaviour {
	private Transform lastSpawnedBlock;
	private float distToCreateNew=60;
	[SerializeField]
	Transform blockPrefab;
//	[SerializeField]
	private float minWidth =0f;//-4;// -2f;
//	[SerializeField]
	private float maxWidth =0;//8;// 0;
//	[SerializeField]
	private float minGap = 0;
//	[SerializeField]
	private float maxGap = 2;
//	[SerializeField]
	private float minHeight = -2f;
//	[SerializeField]
	private float maxHeight = 2f;
	List<GameObject> SpawnedBlock=new List<GameObject>();
	int iSpawnedBlock = 0;
	int curSB;
	void addPool(GameObject obj)
	{
		SpawnedBlock.Add (obj);
		iSpawnedBlock++;
	}
	//	// Use this for initialization
	void Start () {
	if (lastSpawnedBlock == null) {
		Vector3 pos= transform.position;
		pos.x += blockPrefab.localScale.x;

		lastSpawnedBlock = Instantiate (blockPrefab, pos , Quaternion.identity) as Transform;
		lastSpawnedBlock.transform.position = blockPrefab.position;
			addPool (lastSpawnedBlock.gameObject);

	}
	}
	
	// Update is called once per frame
	void Update () {


		if (Vector3.Distance (lastSpawnedBlock.position, Camera.main.transform.position) <= distToCreateNew) {
			float newW = Random.Range (minWidth, maxWidth);//blockPrefab.localScale.x+
			float newGap = Random.Range (minGap, maxGap);
			float newH=blockPrefab.localPosition.y+Random.Range (minHeight, maxHeight);
			Vector3 newPos = new Vector3 (lastSpawnedBlock.localPosition.x + lastSpawnedBlock.localScale.x + newGap + newW, newH, 0);
			//Vector3 newPos = new Vector3 (lastSpawnedBlock.position.x + lastSpawnedBlock.localScale.x * 5 + newGap + newW * 5, newH, 0);
			if (iSpawnedBlock < 10) {
				Menu.Instance.Distance++;
				lastSpawnedBlock = Instantiate (blockPrefab, newPos, Quaternion.identity) as Transform;
				addPool (lastSpawnedBlock.gameObject);
			}
			else {
				curSB++;
				if (curSB >= 10)
					curSB = 0;
				Menu.Instance.Distance++;
				lastSpawnedBlock = SpawnedBlock [curSB].transform;	
				SpawnedBlock [curSB].GetComponent<respawnEnemy> ().refr ();
			}
			
			lastSpawnedBlock.transform.position =newPos;
			lastSpawnedBlock.localScale=new Vector3(blockPrefab.localScale.x+newW,blockPrefab.localScale.y,blockPrefab.localScale.z);
		}
			
	}
}

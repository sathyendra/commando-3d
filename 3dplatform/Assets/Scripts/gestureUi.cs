﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using System.Collections;
using UnityEngine;
using Leap;
using UnityEngine.UI;

public class gestureUi : MonoBehaviour {

		[SerializeField]
		private PlayerMove player;

		public HandController hc;
		private HandModel hm;

		public Text lblNoDeviceDetected;

		public Text lblLeftHandPosition;
		public Text lblLeftHandRotation;

		public Text lblRightHandPosition;
		public Text lblRightHandRotation;

		// Use this for initialization
		void Start()
		{

			hc.GetLeapController().EnableGesture(Gesture.GestureType.TYPE_KEY_TAP);
			hc.GetLeapController().EnableGesture(Gesture.GestureType.TYPESWIPE);
		}

		private GameObject cube = null;

		// Update is called once per frame
		Frame currentFrame;

		Frame lastFrame = null;
		Frame thisFrame = null;
		long difference = 0;

		void Update()
		{

			this.currentFrame = hc.GetFrame();
			GestureList gestures = this.currentFrame.Gestures();
		foreach (Gesture g in gestures) {
			Debug.Log (g.Type);

			if (g.Type == Gesture.GestureType.TYPESWIPE) {
				player.isJump = true;
				//if (this.cube != null)
				//{
				//Destroy(this.cube);
				//this.cube = null;
				//}
			}

			if (g.Type == Gesture.GestureType.TYPE_KEY_TAP) {
				player.isFire = true;
			}
		}
			foreach (var h in hc.GetFrame().Hands)
			{
				if (h.IsRight)
				{
					this.lblRightHandPosition.text = string.Format("Right Hand Position: {0}", h.PalmPosition.ToUnity());
					this.lblRightHandRotation.text = string.Format("Right Hand Rotation: <{0},{1},{2}>", h.Direction.Pitch, h.Direction.Yaw, h.Direction.Roll);

					if (this.cube != null)
						this.cube.transform.rotation = Quaternion.EulerRotation(h.Direction.Pitch, h.Direction.Yaw, h.Direction.Roll);

					foreach (var f in h.Fingers)
					{

						if (f.Type == Finger.FingerType.TYPE_INDEX)
						{
							// this code converts the tip position from leap motion to unity world position
							Leap.Vector position = f.TipPosition;
							Vector3 unityPosition = position.ToUnityScaled(false);
							Vector3 worldPosition = hc.transform.TransformPoint(unityPosition);

							//	string msg = string.Format("Finger ID:{0} Finger Type: {1} Tip Position: {2}", f.Id, f.Type, worldPosition);
							////	Debug.Log(msg);
						}
					}

				}
				if (h.IsLeft)
				{
					this.lblLeftHandPosition.text = string.Format("Left Hand Position: {0}", h.PalmPosition.ToUnity());
					this.lblLeftHandRotation.text = string.Format("Left Hand Rotation: <{0},{1},{2}>", h.Direction.Pitch, h.Direction.Yaw, h.Direction.Roll);

					if (this.cube != null)
						this.cube.transform.rotation = Quaternion.EulerRotation(h.Direction.Pitch, h.Direction.Yaw, h.Direction.Roll);
				}

			}

		}
}

